## Contact

Email: armael DOT gueneau AT ens-lyon DOT fr

Feel free to send me non-formal emails. If you found a bug, have a
request or whatever, just send me an email with a short and raw
description of the problem (you can add extra niceties if you want,
but if it bothers you, don't mind about it :))


