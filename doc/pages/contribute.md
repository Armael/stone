## Contributing to Stone

### Sources

You can get the sources by cloning the dedicated git repository:
{{
    git clone git://git.isomorphis.me/stone.git
}}

You can also access the online cgit interface to the repo:
[git.isomorphis.me/stone/](http://git.isomorphis.me/stone/)

### Sending patches

You have made a modification to *Stone* you are very proud of, and you
want it to be integrated upsteam? No problem  You just have to:
* Format your patches with `git format-patch`. Refer to the
  corresponding man page if needed.
* Send me the patch via [e-mail](contact.html)

